// document.addEventListener('DOMContentLoaded', function() {
//     const content = generateContent();
//     document.getElementById('container').innerHTML = content;
//   });

const content = generateCard("title", "some tex", "GO", "/index.html");
document.getElementById('container').innerHTML = content;

function generateCard(title, text, buttonText, buttonUrl) {
    return `
      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <p class="card-text">${text}</p>
          <a href="${buttonUrl}" class="btn btn-primary">${buttonText}</a>
        </div>
      </div>
    `;
  }

  // ##########################

  const repositoryId = 40985336; // Replace with your repository ID
const apiUrl = `https://gitlab.com/api/v4/projects/${repositoryId}/alerts`;

async function fetchAlerts() {
  try {
    const response = await fetch(apiUrl);
    const alerts = await response.json();
    console.log(typeof(alerts))
    console.log("alerts")
    const list = document.getElementById('alert-list');

    alerts.forEach(alert => {
      const item = document.createElement('li');
      item.textContent = alert.description;
      list.appendChild(item);
    });
  } catch (error) {
    console.error(error);
  }
}

fetchAlerts();

  